package jdbc.lb;


import java.util.Arrays;

import jdbc.lb.dao.PhoneDao;
import jdbc.lb.domain.Phone;


public class Main {
	public static void main(String[] args) {
	PhoneDao dao=new PhoneDao();
	Arrays.asList(
			new Phone("Samsung","Galaxy A7"),
			new Phone("Xiaomi","Mi 8 Lite"),
			new Phone("Meizu","M5 Note")
		)
	.stream()
	.forEach(p->dao.add(p));
	dao.getAll().stream().forEach(System.out::println);
	dao.getAll().stream().findAny().ifPresent(p->{
		p.setBrand("updated");
		dao.update(p);
	});
	dao.getAll().stream().forEach(System.out::println);
	dao.getAll().stream().findAny().ifPresent(p->{
		dao.remove(p.getId());
	});
	dao.getAll().stream().forEach(System.out::println);
 }
}
