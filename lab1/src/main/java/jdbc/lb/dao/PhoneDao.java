package jdbc.lb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jdbc.lb.domain.Phone;

public class PhoneDao implements Dao<Phone, Integer> {
	private final static String url="jdbc:h2:mem:phones";
	public PhoneDao() {
		Connection conn=null;
		try {
			conn=DriverManager.getConnection(url);
			String sql="create table if not exists phones("
						+"id integer primary key auto_increment,"
						+"brand varchar(20),"
						+"model varchar(20))";
			Statement statement=conn.createStatement();
			statement.execute(sql);
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	public List<Phone> getAll() {
		Connection conn=null;
		List<Phone> list=null;
		try {
			conn=DriverManager.getConnection(url);
			String sql="select * from phones";
			Statement statement=conn.createStatement();
			ResultSet rs=statement.executeQuery(sql);
			list=new ArrayList<Phone>();
			while(rs.next()) {
				Phone phone=new Phone();
				phone.setId(rs.getInt(1));
				phone.setBrand(rs.getString(2));
				phone.setModel(rs.getString(3));
				list.add(phone);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public Phone getById(Integer id) {
		Connection conn=null;
		try {
			conn=DriverManager.getConnection(url);
			String sql="select * from products where id= ?";
			PreparedStatement preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			ResultSet rs=preparedStatement.executeQuery();
			while(rs.next()) {
				Phone phone=new Phone();
				phone.setId(rs.getInt(1));
				phone.setBrand(rs.getString(2));
				phone.setModel(rs.getString(3));
				return phone;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Phone add(Phone entity) {
		Connection conn=null;
		try {
			conn=DriverManager.getConnection(url);
			String sql="insert into phones(brand,model) values(?,?)";
			PreparedStatement preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setString(1, entity.getBrand());
			preparedStatement.setString(2,entity.getModel());
			preparedStatement.executeUpdate();
			return entity;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void remove(Integer id) {
		Connection conn=null;
		try {
			conn=DriverManager.getConnection(url);
			String sql="delete from phones where id=?";
			PreparedStatement preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}	
	}

	public void update(Phone entity) {
		Connection conn=null;
		try {
			conn=DriverManager.getConnection(url);
			String sql="update phones set brand=?, model=? where id=?";
			PreparedStatement preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setString(1,entity.getBrand());
			preparedStatement.setString(2, entity.getModel());
			preparedStatement.setInt(3, entity.getId());
			preparedStatement.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}


}
