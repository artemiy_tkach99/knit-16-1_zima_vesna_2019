package jdbc.lb.domain;

public class Phone {
		private int id;
		private String brand;
		private String model;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id=id;
		}
		public String getBrand() {
			return brand;
		}
		public void setBrand(String brand) {
			this.brand=brand;
		}
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model=model;
		}
		public Phone() {}
		public Phone(int id,String brand,String model) {
			this.id=id;
			this.brand=brand;
			this.model=model;
		}
		public Phone(String brand,String model) {
			this.brand=brand;
			this.model=model;
		}
		@Override
		public String toString() {
			return String.format("Product[id=%d, brand=%s, model=%s]",id,brand,model );
		}
}
