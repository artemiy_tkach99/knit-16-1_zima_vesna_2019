package ua.edu.kdu.ius.servletlb;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstHttpServlet
 */
public class FirstHttpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstHttpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuilder resultHTML=new StringBuilder();
		resultHTML.append("<form method=\"POST\">");
		resultHTML.append("<input type=\"submit\" value=\"Show\"/>");
		resultHTML.append("</form>");
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().append(resultHTML);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HashMap<Integer,String> states=new HashMap<Integer,String>();
		states.put(100, "Germany");
		states.put(23, "Spain");
		String str="";
		for(Entry<Integer,String> entry:states.entrySet()) {
			str+=String.format("HashMap[%d]=%s\n", entry.getKey(),entry.getValue());
		}
		response.getWriter().append(str);
	}

}
