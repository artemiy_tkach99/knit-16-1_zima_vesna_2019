package custom.jsp.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.SkipPageException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class TextFormatterTag extends SimpleTagSupport{
	private String text;
	public void setText(String text) {
		this.text=text;
	}
	@Override
	public void doTag() throws JspException, IOException {
		try {
			StringBuilder resultHtml=new StringBuilder();
			resultHtml.append("<p style=\"color:red\">");
			resultHtml.append(text);
			resultHtml.append("</p>");
			getJspContext().getOut().append(resultHtml);
		}catch(Exception e) {
			e.printStackTrace();
			throw new SkipPageException("Exception!");
		}
	}
}
