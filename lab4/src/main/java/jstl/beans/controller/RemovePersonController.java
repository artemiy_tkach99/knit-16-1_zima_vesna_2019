package jstl.beans.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jstl.beans.domain.Person;
import jstl.beans.service.PersonService;

@WebServlet("persons/delete")
public class RemovePersonController extends HttpServlet {
private static final long serialVersionUID = 1L;
	
	private PersonService personService = null;
       
    public RemovePersonController() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id=Integer.parseInt(req.getParameter("id"));
		if(personService==null)
			personService=new PersonService();
		personService.remove(id);
		String ApplicationName = req.getContextPath().replace("/", "");
		resp.sendRedirect("/" + ApplicationName + "/persons");
	}

}
