package jstl.beans.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jstl.beans.domain.Person;
import jstl.beans.service.PersonService;

@WebServlet("persons/edit")
public class UpdatePersonController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PersonService personService = null;
       
    public UpdatePersonController() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(personService==null)
			personService=new PersonService();
		Integer id=Integer.parseInt((req.getParameter("id")));
		Person entity=personService.getById(id);
		req.setAttribute("person", entity);
		getServletContext().getRequestDispatcher("/updateForm.jsp").forward(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(personService==null)
			personService=new PersonService();
		int id=Integer.parseInt(req.getParameter("id"));
		String name=req.getParameter("name");
		int age=Integer.parseInt(req.getParameter("age"));
		String phoneNumber=req.getParameter("phoneNumber");
		Person entity=new Person(id,name,age,phoneNumber);
		personService.update(entity);
		String ApplicationName = req.getContextPath().replace("/", "");
		resp.sendRedirect("/" + ApplicationName + "/persons");
	}
}
