<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="layouts/head.jsp"%>

<h1>Add</h1>

<form method="post" action>
  <input type="hidden" name="id" value="<c:out value='${person.id}'/>"/>
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" placeholder="Name" name="name" value="<c:out value='${person.name}'/>">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Age</label>
    <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Age" name="age" value="<c:out value='${person.age}'/>">
  </div>
  <div class="form-group">
  <label class="form-check-label" for="exampleCheck1">Phone Number</label>
    <input type="text" class="form-control" id="exampleCheck1" name="phoneNumber" placeholder="Phone Number" value="<c:out value='${person.phoneNumber}'/>">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

<%@include file="layouts/footer.jsp"%>