import hibernate.repository.domain.Person;
import hibernate.repository.repository.PersonRepository;
import hibernate.repository.repository.Repository;

public class Main {

	public static void main(String[] args) {
		Repository rep=new PersonRepository();
		Person firstPerson=new Person("Michael",28,"0984054114");
		rep.add(firstPerson);
		Person secondPerson=new Person("Sergey",18,"0954654114");
		rep.add(secondPerson);
		rep.remove(new Long(2));
		firstPerson.setAge(29);
		rep.update(firstPerson);
		rep.getAll().stream().forEach(System.out::println);
	}

}
