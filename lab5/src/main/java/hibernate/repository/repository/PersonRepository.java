package hibernate.repository.repository;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Transaction;

import hibernate.repository.domain.Person;

public class PersonRepository implements Repository<Person, Long> {

	public List<Person> getAll() {
		Transaction tr=HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
		List<Person> list=HibernateUtil.getSessionFactory().getCurrentSession().createQuery("select a from Person a",Person.class).getResultList();
		tr.commit();
		return list;
	}

	public Person getById(Long id) {
		Person p=HibernateUtil.getSessionFactory().getCurrentSession().get(Person.class,id);
		return p;
	}

	public Person add(Person entity) {
		Transaction tr=HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
		HibernateUtil.getSessionFactory().getCurrentSession().save(entity);
		tr.commit();
		return entity;
	}

	public void remove(Long id) {
		Transaction tr=HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
		Query query=HibernateUtil.getSessionFactory().getCurrentSession().createQuery("delete Person where id= :id");
		query.setParameter("id", id);
		query.executeUpdate();
		tr.commit();
	}

	public void update(Person entity) {
		Transaction tr=HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
		HibernateUtil.getSessionFactory().getCurrentSession().update(entity);
		tr.commit();
	}

}
