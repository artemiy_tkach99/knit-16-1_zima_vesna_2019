package hibernate.repository.service;

import java.util.List;

import hibernate.repository.domain.Person;
import hibernate.repository.repository.PersonRepository;

public class PersonService {
	private PersonRepository personRepository;
	public PersonService() {
		personRepository = new PersonRepository();
	}
	
	public List<Person> getAll(){
		return personRepository.getAll();
	}
	
	public Person add(Person entity) {
		return personRepository.add(entity);
	}
	public void remove(Long id) {
		personRepository.remove(id);
	}
	public void update(Person entity) {
		personRepository.update(entity);
	}
	public Person getById(Long id) {
		return personRepository.getById(id);
	}
}
